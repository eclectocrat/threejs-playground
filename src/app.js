// Global state
var renderer, scene, camera, stats;
var particles;
var raycaster, intersects;
var mouse, INTERSECTED;
const PARTICLE_SIZE = 12;

init();
animate();

function initGeometry(scene, count, dims, radius) {
    const inputDims = 3;
    var data = createData(count, inputDims);
	var positions = new Float32Array(count * inputDims);
	var colors = new Float32Array(count * 3);
	var sizes = new Float32Array(count);
	var color = new THREE.Color();
    //var vertex = new THREE.Vector3();
	for (var i = 0, l = count; i < l; i++) {
		//vertex.x = (data[i][0] * 2.0 - 1.0) * radius;
		//vertex.y = (data[i][1] * 2.0 - 1.0) * radius;
		//vertex.z = 1;
        //vertex.toArray(positions, i * inputDims);
        const start = i * inputDims;
        for (var j = start; j < start + inputDims; ++j) {
            positions[j] = (data[i][j - start] * 2.0 - 1.0) * radius;
        }
        positions[start + 2] = 1.0;
		color.setHSL(0.01 + 0.1 * (i / l), 1.0, 0.5);
		color.toArray(colors, i * 3);
		sizes[i] = PARTICLE_SIZE * 0.5;
	}
	var geometry = new THREE.BufferGeometry();
	geometry.setAttribute('position', new THREE.BufferAttribute(positions, inputDims));
	geometry.setAttribute('customColor', new THREE.BufferAttribute(colors, 3));
	geometry.setAttribute('size', new THREE.BufferAttribute(sizes, 1));
	var material = new THREE.ShaderMaterial({
		uniforms: {
			color: { 
                value: new THREE.Color(0xffffff) 
            },
			pointTexture: { 
                value: new THREE.TextureLoader().load("textures/sprites/disc.png") 
            }
		},
		vertexShader: document.getElementById('vertexshader').textContent,
		fragmentShader: document.getElementById('fragmentshader').textContent,
		alphaTest: 0.9
	});
	particles = new THREE.Points(geometry, material);
	scene.add(particles);
}

function init() {
	var container = document.body;
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
	camera.position.z = 250;

    initGeometry(scene, 75000, 2, 100);

	renderer = new THREE.WebGLRenderer();
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	container.appendChild(renderer.domElement);
	raycaster = new THREE.Raycaster();
	mouse = new THREE.Vector2();
    stats = new Stats();
    container.appendChild(stats.dom);
	window.addEventListener('resize', onWindowResize, false);
	document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('keydown', onKeyDown, false);
}

function onDocumentMouseMove(event) {
	event.preventDefault();
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
}

function onKeyDown(event) {
    switch (event.keyCode) {
        case 37: // left
            break;
        case 38: // up
            camera.position.z += 5;
            break;
        case 39: // right
            break;
        case 40: // down
            camera.position.z -= 5;
            break;
    }    
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
	requestAnimationFrame(animate);
	render();
    stats.update();
}

function render() {
	//particles.rotation.x += 0.0005;
	//particles.rotation.y += 0.001;
	var geometry = particles.geometry;
	var attributes = geometry.attributes;

    // Handle mouse intersection (object selection).
	raycaster.setFromCamera(mouse, camera);
	intersects = raycaster.intersectObject(particles);
	if (intersects.length > 0) {
		if (INTERSECTED != intersects[0].index) {
			attributes.size.array[INTERSECTED] = PARTICLE_SIZE;
			INTERSECTED = intersects[0].index;
			attributes.size.array[INTERSECTED] = PARTICLE_SIZE * 1.25;
			attributes.size.needsUpdate = true;
		}
	} else if (INTERSECTED !== null) {
		attributes.size.array[INTERSECTED] = PARTICLE_SIZE;
		attributes.size.needsUpdate = true;
		INTERSECTED = null;
	}

    // Render to screen.
	renderer.render(scene, camera);
}
