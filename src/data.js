
function createData(count, dims, bias) {
    if (!bias) {
        bias = new Float32Array(dims);
        for (var i = 0; i<dims; ++i) {
            bias[i] = 1.0;    
        }
    }
    var output = new Array(count);
    for (var i=0; i<count; ++i) {
        var vector = new Float32Array(dims);
        for (var j=0; j<dims; ++j) {
            vector[j] = Math.random() * bias[j];
        } 
        output[i] = vector;
    }
    return output;
}
